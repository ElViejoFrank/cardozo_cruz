class Control_electrobomba:#Se instancia la clase de alarma
 Ebomba = 0
 def __init__(self,SenL,SenR,SenT,SenS): #Se usa el metodo constructor init para instanciar las variables del sistema de riego.
    self.__SenL = SenL 
    self.SenR = SenR 
    self.SenT = SenT 
    self.SenS = SenS 

 def __lluvia(self): #Se define la funcion privada
  return self.__SenL
   
 def EstadoB(self): #Se define la funcion que realizara la clasificacion segun los valores de los sensores
    if self.__lluvia() >= 5:
      self.Ebomba = 1
      print("La bomba empezo a regar")
    elif self.SenT >= 40:
      self.Ebomba = 1
      print("La bomba empezo a regar")
    elif self.SenT < 40 and self.SenR <= 10 and self.SenS <= 10:
      self.Ebomba = 1
      print("La bomba empezo a regar")
    else:
      self.Ebomba = 0
      print("La bomba no esta funcionando actualmente")
      print("La bomba esta apagada")